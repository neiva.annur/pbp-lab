from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
    note = Note.objects.all().values()
    response = {'notes': note}
    return render(request, 'lab4_index.html', response) #lab4_index???

def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_card.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lab-4')
    else:
        form = NoteForm()
    return render(request, 'lab4_form.html', {'form': form})


