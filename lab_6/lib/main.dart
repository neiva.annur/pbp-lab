import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Safe Flight',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blueGrey,
      ),
      home: const MyHomePage(title: 'Safe Flight'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();

}

class _MyHomePageState extends State<MyHomePage> {
  final formKey = GlobalKey<FormState>(); //key for form
  String name="";

  @override
  Widget build(BuildContext context) {
    final double height= MediaQuery.of(context).size.height;
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
        actions: [
            Padding(
                padding: EdgeInsets.only(right: 10.0),
                child: PopupMenuButton (
                    itemBuilder: (context) => [
                        PopupMenuItem(
                            child: Row(
                                children: [
                                    Padding(
                                        padding: EdgeInsets.all(10),
                                        child: Text('Beranda'),
                                    ), //Padding
                                ],
                            ), //Row
                        ), //PopupMenuItem
                        PopupMenuItem(
                            child: Row(
                                children: [
                                    Padding(
                                        padding: EdgeInsets.all(10),
                                        child: Text('Fitur'),
                                    ), //Padding
                                ],
                            ), //Row
                        ), //PopupMenuItem
                        PopupMenuItem(
                            child: Row(
                                children: [
                                    Padding(
                                        padding: EdgeInsets.all(10),
                                        child: Text('Masuk'),
                                    ), //Padding
                                ],
                            ), //Row
                        ), //PopupMenuItem
                    ],
                    child: Icon(
                        Icons.more_horiz,
                        size: 28.0,
                    ), //Icon
                ), //PopupMenuButton    
            ), //Padding
        ]
      ), //AppBar
      body: Container(
        padding: EdgeInsets.only(top:24.0, left:16.0, right:16.0),
        child: Form(
          key: formKey, //key for form
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: height*0.04),
              Text('Get Swabbed!', 
              style: TextStyle(fontSize:25.0, color: Colors.black), textAlign: TextAlign.center,),
              Text('\nBanyak negara mengharuskan pendatang internasional melakukan tes Swab Covid-19 ketika mereka tiba di negara tersebut. Anda dapat melihat fasilitas penyedia tes Swab Covid-19 di negara tujuan dengan memanfaatkan fitur Get Swabbed! ini.\n\nInformasi lainnya mengenai regulasi di negara tujuan dapat dibaca di regulasi.',
              style: TextStyle(fontSize:16.0, color: Colors.black54), textAlign: TextAlign.center,),
              SizedBox(height: height*0.05,),
              TextFormField(
                decoration: InputDecoration(
                  helperText: 'Pilihan negara tujuan: Malaysia | Singapore | Thailand',
                  labelText: 'Negara Tujuan'
                ), //InputDecoration
                // validator: (value){
                //   if(value!.isEmpty || !RegExp(r'^[a-z A-Z]+$').hasMatch(value!)){
                //     return "Pilih negara yang tersedia";
                //   }else{
                //     return null;
                //   }
                // },
              ) //TextFormField
            ],
          ), //Column
        ), //Form
      ) //Container
      

      //       children: ClipRRect(
      //         borderRadius: BorderRadius.circular(15),
      //         child: Image(
      //           image: AssetImage('assets/traveling 1.jpg',
      //           ), //AssetImage)
      //           width: 400,
      //           height: 300,
      //           fit: BoxFit.cover,
      //         ), //Image
      //       ), //ClipRRect
      //     ), //Column
      //   ); //SafeArea as center
      // ), //Container

        // mainAxisAlignment: MainAxisAlignment.center,
        //   children: <Widget>[
        //     const Text(
        //       'Safe Flight',
        //     ),
      //), //SafeArea
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        // child: Column(
        //   // Column is also a layout widget. It takes a list of children and
        //   // arranges them vertically. By default, it sizes itself to fit its
        //   // children horizontally, and tries to be as tall as its parent.
        //   //
        //   // Invoke "debug painting" (press "p" in the console, choose the
        //   // "Toggle Debug Paint" action from the Flutter Inspector in Android
        //   // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
        //   // to see the wireframe for each widget.
        //   //
        //   // Column has various properties to control how it sizes itself and
        //   // how it positions its children. Here we use mainAxisAlignment to
        //   // center the children vertically; the main axis here is the vertical
        //   // axis because Columns are vertical (the cross axis would be
        //   // horizontal).
        //   mainAxisAlignment: MainAxisAlignment.center,
        //   children: <Widget>[
        //     const Text(
        //       'You have pushed the button this many times:',
        //     ),
        //     Text(
        //       '$_counter',
        //       style: Theme.of(context).textTheme.headline4,
        //     ), //Text()
        //   ], //<Widget>
        // ), //column
      //), //return scaffold
      // floatingActionButton: FloatingActionButton(
      //   onPressed: _incrementCounter,
      //   tooltip: 'Increment',
      //   child: const Icon(Icons.add),
      //
      // ), // This trailing comma makes auto-formatting nicer for build methods.
    ); //Scaffold
  }
}