import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Safe Flight",
    home: GetSwabbedForm(),
    theme: ThemeData(
      primarySwatch: Colors.blueGrey,
    ),
  ));
}

class GetSwabbedForm extends StatefulWidget {
  @override
  _GetSwabbedState createState() => _GetSwabbedState();
}

class _GetSwabbedState extends State<GetSwabbedForm> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;
  String value = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Get Swabbed!'),
        backgroundColor: Colors.blueGrey,
        ),
      body: Center(
          child: Container(
            width: MediaQuery.of(context).size.width/2,
            child: Card(
              child: Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.only(top:24.0, left:16.0, right:16.0),
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top:24.0, left:16.0, right:16.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              //SizedBox(height: height*0.04),
                              Text('Get Swabbed!', 
                              style: TextStyle(fontSize:25.0, color: Colors.black), textAlign: TextAlign.center,),
                              Text('\nBanyak negara mengharuskan pendatang internasional melakukan tes Swab Covid-19 ketika mereka tiba di negara tersebut. Anda dapat melihat fasilitas penyedia tes Swab Covid-19 di negara tujuan dengan memanfaatkan fitur Get Swabbed! ini.\n\nInformasi lainnya mengenai regulasi di negara tujuan dapat dibaca di regulasi.',
                              style: TextStyle(fontSize:16.0, color: Colors.black54), textAlign: TextAlign.center,),
                            ]),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top:24.0, left:16.0, right:16.0),
                          child: TextFormField(
                            decoration: new InputDecoration(
                              hintText: 'Pilihan negara tujuan: Malaysia | Singapore | Thailand',
                              labelText: 'Negara Tujuan',
                              border: OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(20.0)),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Negara tujuan tidak boleh kosong';
                              }
                              return null;
                            },
                          ),
                        ),
   
                        RaisedButton(
                          child: Text(
                            "Submit",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Colors.blueGrey,
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              print("Input negara berhasil diterima!");
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          )
      ),
    );
  }
}
