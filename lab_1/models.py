from django.db import models

class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.IntegerField()
    date_of_birth = models.DateField()