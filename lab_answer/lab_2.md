#1 Perbedaan antara JSON dan XML
____________________________________________________________________________
            JSON                |                   XML
________________________________|___________________________________________            
- JavaScript Object Nation      | - eXtensible Markup Language
- Tidak menggunakan tags        | - Menggunakan tags (start tag dan end tag)      
- Data disimpan seperti map     | - Data disimpan sebagai tree structure
- Mendukung penggunaan array    | - Tidak mendukung penggunaan array
- Hanya men-support data type   | - Men-support data type yang lebih bervariasi
berupa text dan number          |e.g text, number, image, charts, graphs



#2 Perbedaan antara HTML dan XML
____________________________________________________________________________
            HTML                |                   XML
________________________________|___________________________________________  
- Hyper Text Markup Language    | - eXtensible Markup Language
- Tidak case sensitive          | - Case sensitive
- Fungsi: menyajikan data       | - Fungsi: melakukan transport data
- Tidak butuh closing tags      | - Butuh closing tags
- Error kecil dimaklumi         | - Tidak boleh ada error



source:
https://www.javatpoint.com/json-vs-xml
https://www.educba.com/json-vs-xml/
https://www.javatpoint.com/html-vs-xml
https://www.geeksforgeeks.org/html-vs-xml/
